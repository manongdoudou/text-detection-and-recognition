#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2022/2/24 15:25
# @Author  : luocan
# @E-mail  : luott_777@163.com
# @Site    : 
# @File    : binarization.py
# @Software: PyCharm

import matplotlib
import cv2
import matplotlib.pyplot as plt
from skimage.data import page
from skimage.filters import (threshold_otsu, threshold_niblack,
                             threshold_sauvola)

matplotlib.rcParams['font.size'] = 9


def binary_show(img_one, name1, img_two, name2, img_three, name3):
    plt.figure(figsize=(12, 8))

    plt.subplot(1, 3, 1)
    plt.imshow(img_one, cmap=plt.cm.gray)
    plt.title(name1)
    plt.axis('off')

    plt.subplot(1, 3, 2)
    plt.title(name2)
    plt.imshow(img_two, cmap=plt.cm.gray)
    plt.axis('off')

    plt.subplot(1, 3, 3)
    plt.imshow(img_three, cmap=plt.cm.gray)
    plt.title(name3)
    plt.axis('off')

    plt.show()


def binary_show_4(img_one, name1, img_two, name2, img_three, name3, img_four, name4):
    plt.figure(figsize=(6, 4))

    plt.subplot(2, 2, 1)
    plt.imshow(img_one, cmap=plt.cm.gray)
    plt.title(name1)
    plt.axis('off')

    plt.subplot(2, 2, 2)
    plt.title(name2)
    plt.imshow(img_two, cmap=plt.cm.gray)
    plt.axis('off')

    plt.subplot(2, 2, 3)
    plt.imshow(img_three, cmap=plt.cm.gray)
    plt.title(name3)
    plt.axis('off')

    plt.subplot(2, 2, 4)
    plt.imshow(img_four, cmap=plt.cm.gray)
    plt.title(name4)
    plt.axis('off')

    plt.show()


# Sauvola二值化
def binary_sauvola(path):
    image = cv2.imread(path)
    gray = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    window_size = 25
    thresh_sauvola = threshold_sauvola(gray, window_size=window_size)
    binary_sauvola = gray > thresh_sauvola
    return binary_sauvola, gray, image


def show(img, name):
    plt.figure(figsize=(4, 4))
    plt.subplot(1, 1, 1)
    plt.imshow(img, cmap=plt.cm.gray)
    plt.title(name)
    plt.axis('off')
    plt.show()


def binary_compare(path):
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    binary_global = image > threshold_otsu(image)
    window_size = 25
    thresh_niblack = threshold_niblack(image, window_size=window_size, k=0.8)
    thresh_sauvola = threshold_sauvola(image, window_size=window_size)
    binary_niblack = image > thresh_niblack
    binary_sauvola = image > thresh_sauvola
    show(image, ' ')
    show(binary_global, ' ')
    show(binary_niblack, ' ')
    show(binary_sauvola, ' ')


if __name__ == "__main__":
    input_path = "input_photo/3.jpg"
    binary_compare(input_path)

    # img11 = cv2.imread("input_photo/1.jpg")
    # img22 = cv2.imread("input_photo/2.jpg")
    # img33 = cv2.imread("input_photo/3.jpg")
    # binary_show(img11, "for scanning", img22, " for camera", img33, " for mobile phone")
    #
    # img1 = binary_sauvola("input_photo/1.jpg")
    # img2 = binary_sauvola("input_photo/2.jpg")
    # img3 = binary_sauvola("input_photo/3.jpg")
    # binary_show(img1, "for scanning", img2, "for camera", img3, "for mobile phone")
    #
    # # sauvola = binary_sauvola(input_path)
    # # plt.subplot(1, 1, 1)
    # # plt.title('Global Threshold')
    # # plt.imshow(sauvola, cmap=plt.cm.gray)
    # # plt.axis('off')
    # # plt.show()
