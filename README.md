# TextDetectionAndRecognition

#### 介绍

针对银行回执单图像，将融合残差网络与改进的CRNN模型与文本检测模块进行有效的结合，形成一套完整、高效、准确的集成模块。


#### 目的
随着社会的高速发展，企业对外交易产生了大量银行回单凭证，应用于企业内部做账及税务核算等场景。银行回执单需要被快速识别和录入。

#### 原理

post请求-编码-解码-预处理（去噪-校正）-文本检测-文本识别-后处理（文本纠错）-json格式文本行


#### 文件结构


```
+-- base64 (471.14KB)                 #编码解码
+-- checkpoints (110.75MB)            #CTPN和CRNN模型
+-- DatasetGeneration (17.33MB)       #CRNN数据集生成
|   +-- config (337KB)                #配置文件
|   |   +-- demo.py (9.46KB)          #生成语料库
|   +-- fonts (16.78MB)               #选择字体
|   `-- sample_generator.py (1KB)     #生成数据集
+-- deleteFile (1.1MB)                #删除两文件夹中不同文件
+-- demo.py (1.28KB)                  #文本检测与识别多图片测试
+-- detect (25.92KB)                  #ctpn模型预测
+-- IDaRApi.py (8.14KB)               #集成接口
+-- ocr.py (2.99KB)                   #文本检测与识别
+-- others (5.48MB)                   #视频展示
+-- postProcessing (1.65KB)           #后处理
|   +-- confidence.txt (276b)         #置信度
|   `-- textErrorCorrection.py        #文本纠错
+-- preTreatment (3.52MB)             #预处理
|   +-- binarization.py (3.23KB)      #二值化
|   +-- denoising.py (855b)           # 去噪
|   +-- image_correction.py (10.6KB)  # 预处理
+-- recognize (88.07KB)               #crnn模型预测
+-- test (930.12KB)                   #测试
|   +-- test_images (930.12KB)        #检测与识别模型输入
|   `-- test_result                   #检测与识别模型输出
+-- textErrorCorrection.py (1.38KB)   #文本纠错
`-- train_code (257.5MB)              #训练
    +-- train_crnn (43.3MB)           #CRNN模型训练
    `-- train_ctpn (3.54MB)           #CTPN模型训练


```

#### 使用说明
 
 - 运行集成接口
 
```
python IDaRApi.py
```


- 1 接口url
 
```
http://127.0.0.1:9999/receiptIdentification
```

- 2 post请求

银行回执单图片放在post请求的body上，key为img，value为编码后的图片

编码方式在：
```
 ocr_api/base/input.py 
```
- 3 postman测试

![](ocr/others/1.png)

#### 视频展示

接口测试视频路径：

```
ocr/others/接口测试.mp4
```


#### 参与贡献

1.  luocan