# -*- coding: utf-8-*-
# @Time    : 2022/2/16 20:19
# @Author  : luocan
# @E-mail  : luott_777@163.com
# target   : 预处理模块

# 预处理流程
# 输入图像-Sauvola二值化-非局部（Non-local）去噪-霍夫变换获取倾斜角-校正-返回图像
import sys

import numpy as np
import os
import cv2
import math
from scipy import misc, ndimage
# from binarization import binary_sauvola, binary_show, binary_show_4
# from denoising import denoising
# from hough_tilt_angle import rotated_img_with_fft_2
import matplotlib.pyplot as plt

import matplotlib
import cv2
import matplotlib.pyplot as plt
from skimage.data import page
from skimage.filters import (threshold_otsu, threshold_niblack,
                             threshold_sauvola)

matplotlib.rcParams['font.size'] = 9

import cv2
from matplotlib import pyplot as plt

import cv2
import numpy as np
import math
from math import fabs, radians, sin, cos
import matplotlib.pyplot as plt

plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签
plt.rcParams['axes.unicode_minus']=False #用来正常显示负号 #有中文出现的情况，需要u'内容'





def rotated_img_with_fft(gray):
    # 图像延扩
    h, w = gray.shape[:2]
    # 以空间换时间，一般应该是2的n次方，这样便于FFT进行更多层次的二分，从而加快变换速度扩大数值到特定值来加速傅里叶变换
    new_h = cv2.getOptimalDFTSize(h)
    new_w = cv2.getOptimalDFTSize(w)
    right = new_w - w
    bottom = new_h - h
    # 扩充图像的边界，
    img = cv2.copyMakeBorder(gray, 0, bottom, 0, right, borderType=cv2.BORDER_CONSTANT, value=0)

    # 执行傅里叶变换，并过得频域图像
    f = np.fft.fft2(img)
    fshift = np.fft.fftshift(f)

    fft_img = np.log(np.abs(fshift))
    fft_img = (fft_img - np.amin(fft_img)) / (np.amax(fft_img) - np.amin(fft_img))

    fft_img *= 255
    ret, thresh = cv2.threshold(fft_img, 150, 255, cv2.THRESH_BINARY)

    # 霍夫直线变换
    thresh = img.astype(np.uint8)
    lines = cv2.HoughLinesP(img, 1, np.pi / 180, 30, minLineLength=40, maxLineGap=100)
    try:
        lines1 = lines[:, 0, :]
    except Exception as e:
        lines1 = []

    piThresh = np.pi / 180
    pi2 = np.pi / 2
    angle = 0
    for line in lines1:
        x1, y1, x2, y2 = line
        if x2 - x1 == 0:
            continue
        else:
            theta = (y2 - y1) / (x2 - x1)
        if abs(theta) < piThresh or abs(theta - pi2) < piThresh:
            continue
        else:
            angle = abs(theta)
            break

    angle = math.atan(angle)
    angle = angle * (180 / np.pi)

    center = (w // 2, h // 2)
    height_1 = int(w * fabs(sin(radians(angle))) + h * fabs(cos(radians(angle))))
    width_1 = int(h * fabs(sin(radians(angle))) + w * fabs(cos(radians(angle))))

    M = cv2.getRotationMatrix2D(center, angle, 1.0)

    M[0, 2] += (width_1 - w) / 2
    M[1, 2] += (height_1 - h) / 2
    rotated = cv2.warpAffine(gray, M, (width_1, height_1), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

    return rotated


def rotated_img_with_fft_2(path):
    print("----------进入兜底方案-----------")
    thresh, gray, img = binary_sauvola(path)
    # binary_show(thresh, "sauvola-thresh", gray, " gray", img, "img")
    print("----------sauvola二值化-----------")
    show(thresh, "sauvola二值化效果图")


    coords = np.column_stack(np.where(thresh < 1))

    angle = cv2.minAreaRect(coords)[-1]


    if angle < -45:
        angle = -(90 + angle)
    elif angle > 45:
        angle = 90 - angle
    else:
        angle = -angle

    print("-----------获取文本行倾斜角度------------")
    print("倾斜角angle：{}".format(angle))
    h, w = img.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(img, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    # cv2.putText(rotated, "Angle:{:.2f} degrees".format(angle), (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 255), 4)
    show(rotated,"校正后效果图")

    # cv2.imwrite("r1.png",rotated)
    print("----------预处理过程结束-----------")

    return rotated


# 非局部（Non-local）去噪
def denoising(path):
    img = cv2.imread(path)
    dst = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 21)

    # plt.subplot(121), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # plt.xticks([]), plt.yticks([]), plt.title("Origin Image")
    # plt.subplot(122), plt.imshow(cv2.cvtColor(dst, cv2.COLOR_BGR2RGB))
    # plt.xticks([]), plt.yticks([]), plt.title("Non-local Image")
    # plt.show()
    cv2.imwrite("denised00.png",dst)
    return  dst
def binary_show(img_one, name1, img_two, name2, img_three, name3):
    plt.figure(figsize=(12, 8))

    plt.subplot(1, 3, 1)
    plt.imshow(img_one, cmap=plt.cm.gray)
    plt.title(name1)
    plt.axis('off')

    plt.subplot(1, 3, 2)
    plt.title(name2)
    plt.imshow(img_two, cmap=plt.cm.gray)
    plt.axis('off')

    plt.subplot(1, 3, 3)
    plt.imshow(img_three, cmap=plt.cm.gray)
    plt.title(name3)
    plt.axis('off')

    plt.show()


def binary_show_4(img_one, name1, img_two, name2, img_three, name3, img_four, name4):
    plt.figure(figsize=(6, 4))

    plt.subplot(2, 2, 1)
    plt.imshow(img_one, cmap=plt.cm.gray)
    plt.title(name1)
    plt.axis('off')

    plt.subplot(2, 2, 2)
    plt.title(name2)
    plt.imshow(img_two, cmap=plt.cm.gray)
    plt.axis('off')

    plt.subplot(2, 2, 3)
    plt.imshow(img_three, cmap=plt.cm.gray)
    plt.title(name3)
    plt.axis('off')

    plt.subplot(2, 2, 4)
    plt.imshow(img_four, cmap=plt.cm.gray)
    plt.title(name4)
    plt.axis('off')

    plt.show()


# Sauvola二值化
def binary_sauvola(path):
    image = cv2.imread(path)
    gray = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    window_size = 25
    thresh_sauvola = threshold_sauvola(gray, window_size=window_size)
    binary_sauvola = gray > thresh_sauvola
    return binary_sauvola, gray, image


def show(img, name):
    plt.figure(figsize=(8,8))
    plt.subplot(1, 1, 1)
    plt.imshow(img, cmap=plt.cm.gray)
    plt.title(name,fontsize='xx-large',fontweight='heavy')
    plt.axis('off')
    plt.show()


def binary_compare(path):
    image = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    binary_global = image > threshold_otsu(image)
    window_size = 25
    thresh_niblack = threshold_niblack(image, window_size=window_size, k=0.8)
    thresh_sauvola = threshold_sauvola(image, window_size=window_size)
    binary_niblack = image > thresh_niblack
    binary_sauvola = image > thresh_sauvola
    show(image, ' ')
    show(binary_global, ' ')
    show(binary_niblack, ' ')
    show(binary_sauvola, ' ')

def rotate(image, angle, center=None, scale=1.0):
    (w, h) = image.shape[0:2]
    if center is None:
        center = (w // 2, h // 2)
    wrapMat = cv2.getRotationMatrix2D(center, angle, scale)
    return cv2.warpAffine(image, wrapMat, (h, w), borderValue=(255, 255, 255))


# 边缘检测
def getCanny(input_path):
    # 灰度化
    gray = cv2.imread(input_path, cv2.IMREAD_GRAYSCALE)
    # showAndWaitKey(" gray", gray)
    # 腐蚀、膨胀
    kernel = np.ones((5, 5), np.uint8)
    erode_Img = cv2.erode(gray, kernel)
    eroDil = cv2.dilate(erode_Img, kernel)
    # showAndWaitKey("eroDil", eroDil)
    # 边缘检测
    canny = cv2.Canny(eroDil, 50, 150)
    return canny


# 获取列表的第二个元素
def takeSecond(elem):
    return elem[1]


# 预处理
def getCorrect(input_path):
    print("----------预处理过程开始-----------")
    # 读取图片
    src = cv2.imread(input_path)
    show(src, "原图")
    # 非局部（Non-local）去噪
    denoise = cv2.fastNlMeansDenoisingColored(src, None, 10, 10, 7, 21)
    # cv2.imwrite("result_photo/d1.png", denoise)
    print("----------非局部（Non-local）去噪-----------")
    show(denoise, "非局部去噪效果图")

    # 灰度化
    gray = cv2.cvtColor(denoise, cv2.COLOR_BGR2GRAY)
    # 腐蚀、膨胀
    kernel = np.ones((5, 5), np.uint8)
    erode_Img = cv2.erode(gray, kernel)
    eroDil = cv2.dilate(erode_Img, kernel)
    # 对比展示
    # binary_show(gray, " ")
    # binary_show(eroDil, "  ")

    # 边缘检测
    canny = cv2.Canny(eroDil, 50, 150)

    # 霍夫变换得到线条
    point=src.shape[0]/3
    lines = cv2.HoughLinesP(canny, 0.8, np.pi / 180, 90, minLineLength=point, maxLineGap=10)
    drawing = np.zeros(src.shape[:], dtype=np.uint8)


    # 无线条 采用方案二
    if lines is None:
        print("----------霍夫变换并未检测到直线---------")
        return rotated_img_with_fft_2(input_path)

    print("----------霍夫变换检测到直线---------")
    # print('此输入图像霍夫变换产生{}条直线'.format(len(lines)))
    # 画出线条
    for line in lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(drawing, (x1, y1), (x2, y2), (0, 255, 0), 3, lineType=cv2.LINE_AA)

    """
    计算角度,因为x轴向右，y轴向下，所有计算的斜率是常规下斜率的相反数，我们就用这个斜率（旋转角度）进行旋转
    """
    x1, y1, x2, y2 = lines[0][0]
    k = float(y1 - y2) / (x1 - x2)
    thera = np.degrees(math.atan(k))

    if thera > 45:
        thera = 90 - thera
    elif thera < -45:
        thera = 90 + thera
    show(drawing, "检测直线图")
    print("-----------获取文本行倾斜角度------------")
    print("倾斜角thera为：{}".format(thera))


    """
    旋转角度大于0，则逆时针旋转，否则顺时针旋转
    """
    rotateImg = rotate(src, thera)
    # binary_show(canny, " ")

    show(rotateImg, "校正效果图")
    print("----------预处理过程结束-----------")
    return rotateImg

    # cv2.imwrite("result_photo/r1.png", rotateImg)


def showAndWaitKey(winName, img):
    cv2.imshow(winName, img)
    cv2.waitKey()


def binary_show(img_one, name1):
    plt.figure(figsize=(4, 4))

    plt.subplot(1, 1, 1)
    plt.imshow(img_one, cmap=plt.cm.gray)
    plt.title(name1)
    plt.axis('off')

    plt.show()


if __name__ == "__main__":

    #测试预处模块
    input_path1 = "pretreatment-Test-one.png"
    input_path2 = "pretreatment-Test-two.png"
    print("测试1")
    print("--------原图中有明显直线------")
    n1 = getCorrect(input_path1)
    print("------------------------------")
    print("测试2")
    print("--------原图中无明显直线------")
    n1 = getCorrect(input_path2)
