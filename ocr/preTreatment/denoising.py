#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2022/2/24 16:51
# @Author  : luocan
# @E-mail  : luott_777@163.com
# @Site    : 
# @File    : denoising.py
# @Software: PyCharm
import cv2
from matplotlib import pyplot as plt

# 非局部（Non-local）去噪
def denoising(path):
    img = cv2.imread(path)
    dst = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 21)

    # plt.subplot(121), plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    # plt.xticks([]), plt.yticks([]), plt.title("Origin Image")
    # plt.subplot(122), plt.imshow(cv2.cvtColor(dst, cv2.COLOR_BGR2RGB))
    # plt.xticks([]), plt.yticks([]), plt.title("Non-local Image")
    # plt.show()
    cv2.imwrite("denised00.png",dst)
    return  dst


if __name__ == "__main__":
    input_path = "input_photo/old.png"
    denoising(input_path)
