#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2022/3/4 13:23
# @Author  : luocan
# @E-mail  : luott_777@163.com
# @Site    : 
# @File    : enhanceImg.py
# @Software: PyCharm

# 数据集-------图像增强：加噪、模糊、颜色增强


from PIL import Image
from PIL import ImageEnhance
import numpy as np
import random
import matplotlib.pyplot as plt
import cv2


def sp_noise(image, prob):
    '''
    添加椒盐噪声
    prob:噪声比例
    '''
    output = np.zeros(image.shape, np.uint8)
    thres = 1 - prob
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            rdn = random.random()
            if rdn < prob:
                output[i][j] = 0
            elif rdn > thres:
                output[i][j] = 255
            else:
                output[i][j] = image[i][j]
    return output


def gasuss_noise(image, mean=0, var=0.001):
    '''
        添加高斯噪声
        mean : 均值
        var : 方差
    '''
    image = np.array(image / 255, dtype=float)
    noise = np.random.normal(mean, var ** 0.5, image.shape)
    out = image + noise
    if out.min() < 0:
        low_clip = -1.
    else:
        low_clip = 0.
    out = np.clip(out, low_clip, 1.0)
    out = np.uint8(out * 255)
    # cv.imshow("gasuss", out)
    return out


def img_show_1(img_one, name1):
    plt.figure(figsize=(4, 4))
    plt.subplot(1, 1, 1)
    plt.imshow(img_one, cmap=plt.cm.gray)
    plt.title(name1)
    plt.axis('off')
    plt.show()


# 亮度增强
def brightness_enhancement(image):
    enh_bri = ImageEnhance.Brightness(image)
    brightness = 1.5
    image_brightened = enh_bri.enhance(brightness)
    return image_brightened


# 色度增强
def chroma_enhancement(image):
    enh_col = ImageEnhance.Color(image)
    color = 1.5
    image_colored = enh_col.enhance(color)
    return image_colored


# 对比度增强
def contrast_enhancement(image):
    enh_con = ImageEnhance.Contrast(image)
    contrast = 1.5
    image_contrasted = enh_con.enhance(contrast)
    return image_contrasted


# 锐度增强
def sharpness_enhancement(image):
    enh_sha = ImageEnhance.Sharpness(image)
    sharpness = 3.0
    image_sharped = enh_sha.enhance(sharpness)
    return image_sharped


if __name__ == "__main__":
    input_path = "input_photo/1.jpg"
    src = cv2.imread(input_path)
    noise = sp_noise(src, 0.1)
    img_show_1(noise, "")
    cv2.imwrite("out_img/sp_noise.png", noise)
    gasuss = gasuss_noise(src)
    img_show_1(gasuss, "")
    cv2.imwrite("out_img/gasuss_noise.png", gasuss)
    src = Image.open("input_photo/1.jpg")
    light = brightness_enhancement(src)
    img_show_1(light, "")
    # cv2.imwrite("out_img/brightness_enhancement.png", light)
    enhancement = chroma_enhancement(src)
    img_show_1(enhancement, "")
    # cv2.imwrite("out_img/chroma_enhancement.png", enhancement)
    contrast = contrast_enhancement(src)
    img_show_1(contrast, "")
    # cv2.imwrite("out_img/contrast_enhancement.png", contrast)
    sharpness = sharpness_enhancement(src)
    img_show_1(sharpness, "")
    # cv2.imwrite("out_img/sharpness_enhancement.png", sharpness)
