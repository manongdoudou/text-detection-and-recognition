# -*- coding:utf-8 -*-
# '''
# Created on 18-12-11 上午10:01
#
# @Author: Greg Gao(laygin)
# '''
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
import config


class RPN_REGR_Loss(nn.Module):
    def __init__(self, device, sigma=9.0):
        super(RPN_REGR_Loss, self).__init__()
        self.sigma = sigma
        self.device = device

    def forward(self, input, target):
        '''
        smooth L1 loss
        :param input:y_preds
        :param target: y_true
        :return:
        '''
        try:
            cls = target[0, :, 0]
            regr = target[0, :, 1:3]
            # apply regression to positive sample
            regr_keep = (cls == 1).nonzero()[:, 0]
            regr_true = regr[regr_keep]
            regr_pred = input[0][regr_keep]
            diff = torch.abs(regr_true - regr_pred)
            less_one = (diff < 1.0 / self.sigma).float()
            loss = less_one * 0.5 * diff ** 2 * self.sigma + torch.abs(1 - less_one) * (diff - 0.5 / self.sigma)
            loss = torch.sum(loss, 1)
            loss = torch.mean(loss) if loss.numel() > 0 else torch.tensor(0.0)
        except Exception as e:
            print('RPN_REGR_Loss Exception:', e)
            # print(input, target)
            loss = torch.tensor(0.0)

        return loss.to(self.device)


class RPN_CLS_Loss(nn.Module):
    def __init__(self, device):
        super(RPN_CLS_Loss, self).__init__()
        self.device = device
        self.L_cls = nn.CrossEntropyLoss(reduction='none')
        # self.L_regr = nn.SmoothL1Loss()
        # self.L_refi = nn.SmoothL1Loss()
        self.pos_neg_ratio = 3

    def forward(self, input, target):
        if config.OHEM:
            cls_gt = target[0][0]
            num_pos = 0
            loss_pos_sum = 0

            # print(len((cls_gt == 0).nonzero()),len((cls_gt == 1).nonzero()))

            if len((cls_gt == 1).nonzero()) != 0:  # avoid num of pos sample is 0
                cls_pos = (cls_gt == 1).nonzero()[:, 0]
                gt_pos = cls_gt[cls_pos].long()
                cls_pred_pos = input[0][cls_pos]
                # print(cls_pred_pos.shape)
                loss_pos = self.L_cls(cls_pred_pos.view(-1, 2), gt_pos.view(-1))
                loss_pos_sum = loss_pos.sum()
                num_pos = len(loss_pos)

            cls_neg = (cls_gt == 0).nonzero()[:, 0]
            gt_neg = cls_gt[cls_neg].long()
            cls_pred_neg = input[0][cls_neg]

            loss_neg = self.L_cls(cls_pred_neg.view(-1, 2), gt_neg.view(-1))
            loss_neg_topK, _ = torch.topk(loss_neg, min(len(loss_neg), config.RPN_TOTAL_NUM - num_pos))
            loss_cls = loss_pos_sum + loss_neg_topK.sum()
            loss_cls = loss_cls / config.RPN_TOTAL_NUM
            return loss_cls.to(self.device)
        else:
            y_true = target[0][0]
            cls_keep = (y_true != -1).nonzero()[:, 0]
            cls_true = y_true[cls_keep].long()
            cls_pred = input[0][cls_keep]
            loss = F.nll_loss(F.log_softmax(cls_pred, dim=-1),
                              cls_true)  # original is sparse_softmax_cross_entropy_with_logits
            # loss = nn.BCEWithLogitsLoss()(cls_pred[:,0], cls_true.float())  # 18-12-8
            loss = torch.clamp(torch.mean(loss), 0, 10) if loss.numel() > 0 else torch.tensor(0.0)
            return loss.to(self.device)


class basic_conv(nn.Module):
    # basic_conv(256, 512, 1, 1, relu=True, bn=False)
    def __init__(self,
                 in_planes,
                 out_planes,
                 kernel_size,
                 stride=1,
                 padding=0,
                 dilation=1,
                 groups=1,
                 relu=True,
                 bn=True,
                 bias=True):
        super(basic_conv, self).__init__()
        self.out_channels = out_planes
        self.conv = nn.Conv2d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, padding=padding,
                              dilation=dilation, groups=groups, bias=bias)
        self.bn = nn.BatchNorm2d(out_planes, eps=1e-5, momentum=0.01, affine=True) if bn else None
        self.relu = nn.ReLU(inplace=True) if relu else None

    def forward(self, x):
        x = self.conv(x)
        if self.bn is not None:
            x = self.bn(x)
        if self.relu is not None:
            x = self.relu(x)
        return x


class CTPN_Model(nn.Module):
    def __init__(self):
        super().__init__()
        # 卷积层 取特征 pretrained=True是指使用预训练的权重，否则从新训练

        base_model = models.vgg16(pretrained=False)

        # base_model =models.resnet34(pretrained=False)
        # 转化成列表
        layers = list(base_model.features)[:-1]

        # 收集好参数，一起放进元组里面。 torch.nn.Sequential类来实现简单的顺序连接模型
        self.base_layers = nn.Sequential(*layers)  # block5_conv3 output
        # rpn ：卷积层3*3 保证特征个数不变
        self.rpn = basic_conv(512, 512, 3, 1, 1, bn=False)

        # 双向rnn 也就是Blstm  128输出特征维个数 双向lstm得 128*2
        # bidirectional：True则为双向lstm默认为False
        # batch_first：True则输入输出的数据格式为(batch, seq, feature)
        self.brnn = nn.GRU(512, 128, bidirectional=True, batch_first=True)
        # fc全连接
        self.lstm_fc = basic_conv(256, 512, 1, 1, relu=True, bn=False)
        # 分类器：10个框做二分类 y h做偏移量
        self.rpn_class = basic_conv(512, 10 * 2, 1, 1, relu=False, bn=False)
        # 回归： 10个框做二分类
        self.rpn_regress = basic_conv(512, 10 * 2, 1, 1, relu=False, bn=False)

    # 前向传播
    def forward(self, x):
        # [b, c, h, w] batch channel 图片的h、w
        # 形状 (1,3,h,w)
        # print ('1:',x.size())

        # 经过VGG
        # 形状 (1,512,h,w)
        x = self.base_layers(x)
        # print ('2:',x.size())

        # 经过rpn卷积层
        # 形状 (1,512,h,w)
        x = self.rpn(x)  # [b, c, h, w]
        # print ('3:',x.size())

        # 将(c,h,w)转换成(h,w,c) 以便进行RNN
        # 形状 (1,h,w，512)
        x1 = x.permute(0, 2, 3, 1).contiguous()  # channels last   [b, h, w, c]
        # print ('4:',x1.size())
        b = x1.size()  # b, h, w, c

        # 将h*w切分为 （1-w）*h
        # 也就是(b,h,w,c)转换成(b*h,w,c) 以便进行RNN
        # 形状 (h,w，512)
        x1 = x1.view(b[0] * b[1], b[2], b[3])
        # print ('5:',x1.size())

        # BLSTM   输出c=128*2
        # 形状 (h,w，256)
        x2, _ = self.brnn(x1)
        # print ('6:',x2.size())
        xsz = x.size()

        # 类似resize 到(b,h,w,c)  因为做全连接或者卷积必须转化到channels first
        # 形状 (1，h,w,256)
        x3 = x2.view(xsz[0], xsz[2], xsz[3], 256)
        # print ('7:',x3.size())

        # 转换到 channels first [b, c, h, w]
        # 形状 (1,256，h,w)
        x3 = x3.permute(0, 3, 1, 2).contiguous()
        # print ('8:',x3.size())

        # 全连接
        # 形状 (1,512，h,w)
        x3 = self.lstm_fc(x3)
        # print('9:', x3.size())
        x = x3

        # 分类器：10个框做二分类 y h做偏移量
        # 形状 (1,20，h,w)
        cls = self.rpn_class(x)
        # print('10:', cls.size())

        # 回归： 10个框做二分类
        # 形状 (1,20，h,w)
        regr = self.rpn_regress(x)
        # print('11:', regr.size())

        # 维度换位  (p,c,h,w)转换为(p,h,w,c)
        cls = cls.permute(0, 2, 3, 1).contiguous()
        regr = regr.permute(0, 2, 3, 1).contiguous()

        # 分类器分类结果
        # 形状 (1,10*h*w，2)
        cls = cls.view(cls.size(0), cls.size(1) * cls.size(2) * 10, 2)
        # print('12:', cls.size())

        # 回归结果
        # 形状 (1,10*h*w，2)
        regr = regr.view(regr.size(0), regr.size(1) * regr.size(2) * 10, 2)
        # print('13:', regr.size())

        return cls, regr
