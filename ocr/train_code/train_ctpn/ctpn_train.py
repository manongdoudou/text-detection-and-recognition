# -*- coding:utf-8 -*-
import os

# os.environ['CUDA_VISIBLE_DEVICES'] = '3'
import torch
from torch.utils.data import DataLoader
from torch import optim
import numpy as np
import argparse
from pandas import DataFrame
import matplotlib.pyplot as plt

import config
from ctpn_model import CTPN_Model, RPN_CLS_Loss, RPN_REGR_Loss
from data.dataset import ICDARDataset

# dataset_download:https://rrc.cvc.uab.es/?ch=8&com=downloads
random_seed = 2019
torch.random.manual_seed(random_seed)
np.random.seed(random_seed)

epochs = 2
lr = 1e-3
resume_epoch = 0


def save_checkpoint(state, epoch, loss_cls, loss_regr, loss, ext='pth'):
    check_path = os.path.join(config.checkpoints_dir,
                              f'v3_ctpn-vgg16_ep{epoch:02d}_'
                              f'{loss_cls:.4f}_{loss_regr:.4f}_{loss:.4f}.{ext}')

    try:
        torch.save(state, check_path)
    except BaseException as e:
        print(e)
        print('fail to save to {}'.format(check_path))
    print('saving to {}'.format(check_path))


def weights_init(m):
    """设计初始化函数"""
    classname = m.__class__.__name__
    # print(classname)
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        # bn层里初始化γ，服从（1，0.02）的正态分布
        m.weight.data.normal_(1.0, 0.02)
        # bn层里初始化β，默认为0
        m.bias.data.fill_(0)


if __name__ == '__main__':
    print(torch.cuda.is_available())
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    # pretrained_weights = 'checkpoints/v3_ctpn_ep22_0.3801_0.0971_0.4773.pth'
    checkpoints_weight = config.pretrained_weights
    print('exist pretrained ', os.path.exists(checkpoints_weight))
    if os.path.exists(checkpoints_weight):
        pretrained = False

    dataset = ICDARDataset(config.icdar17_mlt_img_dir, config.icdar17_mlt_gt_dir)

    dataloader = DataLoader(dataset, batch_size=1, shuffle=True, num_workers=config.num_workers)

    model = CTPN_Model()
    model.to(device)

    if os.path.exists(checkpoints_weight):
        print('using pretrained weight: {}'.format(checkpoints_weight))
        cc = torch.load(checkpoints_weight, map_location=device)
        model.load_state_dict(cc['model_state_dict'])
        resume_epoch = cc['epoch']
    else:
        # 递归调用weights_init
        model.apply(weights_init)

    params_to_uodate = model.parameters()
    # 优化器
    optimizer = optim.SGD(params_to_uodate, lr=lr, momentum=0.9)

    critetion_cls = RPN_CLS_Loss(device)
    critetion_regr = RPN_REGR_Loss(device)

    best_loss_cls = 100
    best_loss_regr = 100
    best_loss = 100
    best_model = None
    epochs += resume_epoch
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)

    # 存储loss  数组 config.length*3
    # batch    loss_cls   loss_regr   loss
    loss_batch = [[0 for col in range(1)] for row in range(3)]
    # Epoch    loss_cls   loss_regr   loss
    loss_Epoch = [[0 for col in range(1)] for row in range(3)]

    import time

    plt.ion()
    loss_data = []
    times = [0]

    for epoch in range(resume_epoch + 1, epochs):
        time_start = time.time()
        print(f'Epoch {epoch}/{epochs}')
        print('#' * 50)
        epoch_size = len(dataset) // 1
        model.train()
        epoch_loss_cls = 0
        epoch_loss_regr = 0
        epoch_loss = 0
        scheduler.step(epoch)

        for batch_i, (imgs, clss, regrs) in enumerate(dataloader):
            # print(imgs.shape)
            imgs = imgs.to(device)
            clss = clss.to(device)
            regrs = regrs.to(device)

            optimizer.zero_grad()

            out_cls, out_regr = model(imgs)
            loss_cls = critetion_cls(out_cls, clss)
            loss_regr = critetion_regr(out_regr, regrs)

            loss = loss_cls + loss_regr  # total loss
            print("loss:{}".format(loss))
            loss.backward()
            optimizer.step()

            epoch_loss_cls += loss_cls.item()
            epoch_loss_regr += loss_regr.item()
            epoch_loss += loss.item()
            mmp = batch_i + 1
            print(f'Ep:{epoch}/{epochs - 1}--'
                  f'Batch:{batch_i}/{epoch_size}\n'
                  f'batch: loss_cls:{loss_cls.item():.4f}--loss_regr:{loss_regr.item():.4f}--loss:{loss.item():.4f}\n'
                  f'Epoch: loss_cls:{epoch_loss_cls / mmp:.4f}--loss_regr:{epoch_loss_regr / mmp:.4f}--'
                  f'loss:{epoch_loss / mmp:.4f}\n')

            loss_batch[0].append(loss_cls.item())
            loss_batch[1].append(loss_regr.item())
            loss_batch[2].append(loss.item())
            loss_Epoch[0].append(epoch_loss_cls / mmp)
            loss_Epoch[1].append(epoch_loss_regr / mmp)
            loss_Epoch[2].append(epoch_loss / mmp)

            time_end = time.time()
            print('Time cost = %fs' % (time_end - time_start))
            # 可视化训练过程
            with torch.no_grad():  # 关闭梯度跟踪，防止显存爆炸

                if batch_i % 10 == 0:  # 每一百次训练添加一个点，这样横坐标的数值乘上100才是实际的训练次数，当然也可以每次训练都添加点
                    loss_data.append(loss.cpu().tolist())  # loss是返回的交叉熵，是一个长度为1的tensor，用cpu()放到cpu上，用tolist()转为一个list元素
                    times.append(times[len(times) - 1] + 1)  # 自动递增次数
                    plt.cla()  # 清空上一次画的图
                    # plt.plot(loss_data, 'r-', lw=1)  # 直接输入y轴坐标，不输入x轴坐标是可以的，这样的话x轴坐标会自动生成
                    plt.plot(times[1:], loss_data, 'r-', lw=1)  # x轴的list和y轴的list需要原始的数据总量是一样的，比如x中有2个元素，y中有1个元素，这样是不行的
                    plt.ylabel('Loss')
                    plt.title('loss=%.4f step=%d' % (loss.cpu(), batch_i))
                    plt.pause(0.1)


        epoch_loss_cls /= epoch_size
        epoch_loss_regr /= epoch_size
        epoch_loss /= epoch_size
        print(f'Epoch:{epoch}--{epoch_loss_cls:.4f}--{epoch_loss_regr:.4f}--{epoch_loss:.4f}')
        if best_loss_cls > epoch_loss_cls or best_loss_regr > epoch_loss_regr or best_loss > epoch_loss:
            best_loss = epoch_loss
            best_loss_regr = epoch_loss_regr
            best_loss_cls = epoch_loss_cls
            best_model = model
            save_checkpoint({'model_state_dict': best_model.state_dict(),
                             'epoch': epoch},
                            epoch,
                            best_loss_cls,
                            best_loss_regr,
                            best_loss)

    print("loss_data:{}".format(loss_data))
    plt.savefig("loss.png")
    if torch.cuda.is_available():
        torch.cuda.empty_cache()

    # df = DataFrame(loss_batch)
    # df1 = DataFrame(loss_Epoch)
    # df.to_excel('loss_batch.xlsx')
    # df1.to_excel('loss_Epoch.xlsx')
