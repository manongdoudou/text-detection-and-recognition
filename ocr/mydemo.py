from recognize.crnn_recognizer import PytorchOcr
import cv2
from detect.ctpn_predict import get_det_boxes
from detect.ctpn_predict import dis
from detect.ctpn_utils import resize
height = 720
import numpy as np
from detect import config
import torch
import matplotlib.pyplot as plt



plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号 #有中文出现的情况，需要u'内容'

def show(img, name):
    plt.figure(figsize=(4, 4))
    plt.subplot(1, 1, 1)
    plt.imshow(img, cmap=plt.cm.gray)
    # plt.title("文字识别")
    # name="识别结果："+"说备编号：500C0564"
    name = "识别结果：" + name
    plt.text(200,70,name,color="r",fontsize=13, style='oblique', ha='center',va='top',wrap=True)
    plt.axis('off')
    plt.show()

if __name__ == '__main__':

# 测试CTPN

    # img_path = 'test/test_images/t2.png'
    # image = cv2.imread(img_path)
    # text, image ,im= get_det_boxes(image)
    # dis(image)


# 测试CRNN

    model_path = 'checkpoints/CRNN-1010.pth'
    recognizer = PytorchOcr(model_path)

    img_path = 'test/crnntest/0-3.png'

    img = cv2.imread(img_path)
    h, w = img.shape[:2]
    res = recognizer.recognize(img)
    print(res)
    show(img,res)



