# TextDetectionAndRecognition

#### Description
随着社会的高速发展，企业对外交易产生了大量银行回单凭证，应用于企业内部做账及税务核算等场景。银行回执单需要被快速识别和录入，而在识别过程中存在几个问题。其一，当银行回执单图像存在污渍、噪音干扰、文本行倾斜的极端情况下时，其识别的准确度较差；其二，银行回执单图像整体的识别效果存在一定的改进空间。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
