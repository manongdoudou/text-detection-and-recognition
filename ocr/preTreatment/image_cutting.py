#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2022/2/17 16:00
# @Author  : luocan
# @E-mail  : luott_777@163.com
# @Site    : 预处理：根据单元格切割输入图像
# @File    : image_cutting.py
# @Software: PyCharm

import numpy as np
import os
import cv2
import math
from scipy import misc, ndimage


# 图片切割
def getCutting(input_path):
    # 读取图片
    src = cv2.imread(input_path, cv2.IMREAD_COLOR)
    # showAndWaitKey("src", src)
    canny = getCanny(input_path)
    # showAndWaitKey("canny", canny)
    # 霍夫变换得到线条
    lines = cv2.HoughLinesP(canny, 0.8, np.pi / 180, 90, minLineLength=400, maxLineGap=10)
    drawing = np.zeros(src.shape[:], dtype=np.uint8)
    if len(lines)<1:
        return
    x1_before, y1_before, x2_before, y2_before = lines[0][0]
    print('1_before:{}  2_before:{}  3_before:{}  4_before:{}'.format(x1_before, y1_before, x2_before, y2_before))
    # 记录切割点的位置，也就是y1
    cut_point = []
    # 画出线条
    newLine = []
    for line in lines:
        x1, y1, x2, y2 = line[0]
        if y1 == y2:
            newLine.append(line[0].tolist())
    print('-------横线列表---------')
    print(newLine)
    newLine.sort(key=takeSecond)
    print('--------有序横线列表---------')
    print(newLine)

    # cv2.line(drawing, (x1, y1), (x2, y2), (0, 255, 0), 1, lineType=cv2.LINE_AA)
    for line in newLine:
        x1, y1, x2, y2 = line
        # print('1:{}  2:{}  3:{}  4:{}'.format(x1, y1, x2, y2))
        if y1_before - y1 > 120 or y1 - y1_before > 120:
            cut_point.append(y1)
            # cv2.line(drawing, (x1, y1), (x2, y2), (0, 255, 0), 1, lineType=cv2.LINE_AA)
            # cv2.line(src, (x1, y1), (x2, y2), (0, 255, 0), 1, lineType=cv2.LINE_AA)
            x1_before, y1_before, x2_before, y2_before = line
    # showAndWaitKey("houghP", drawing)
    showAndWaitKey("src", src)
    fir = 0
    cur = 0
    print(cut_point)
    for i in range(len(cut_point) + 1):
        if i==len(cut_point):
            pointImg = src[fir:, :]
        else:
            cur = cut_point[i]
            pointImg = src[fir:cur, :]

        fir = cur
        cv2.imwrite("result_photo/res_point_"+str(i)+".png", pointImg)


# 边缘检测
def getCanny(input_path):
    # 灰度化
    gray = cv2.imread(input_path, cv2.IMREAD_GRAYSCALE)
    # showAndWaitKey(" gray", gray)
    # 腐蚀、膨胀
    kernel = np.ones((5, 5), np.uint8)
    erode_Img = cv2.erode(gray, kernel)
    eroDil = cv2.dilate(erode_Img, kernel)
    # showAndWaitKey("eroDil", eroDil)
    # 边缘检测
    canny = cv2.Canny(eroDil, 50, 150)
    return canny


def showAndWaitKey(winName, img):
    cv2.imshow(winName, img)
    cv2.waitKey()


def rotate(image, angle, center=None, scale=1.0):
    (w, h) = image.shape[0:2]
    if center is None:
        center = (w // 2, h // 2)
    wrapMat = cv2.getRotationMatrix2D(center, angle, scale)
    return cv2.warpAffine(image, wrapMat, (h, w))


# 获取列表的第二个元素
def takeSecond(elem):
    return elem[1]


if __name__ == "__main__":
    input_path = "result_photo/img_1.png"
    getCutting(input_path)
