#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2022/3/27 11:02
# @Author  : luocan
# @E-mail  : luott_777@163.com
# @Site    : 
# @File    : IDaRApi.py
# @Software: PyCharm
from flask import Flask
from flask import jsonify
import pandas as pd
import numpy as np
import json
from flask import request
import demjson
import joblib  # 直接导入，已经从sklearn中独立

import os

from ocr import ocr
import base64
import cv2

import time
import shutil
import numpy as np
from PIL import Image
from glob import glob
import datetime
# coding=utf-8
import os
from ocr import ocr
import time
import shutil
import numpy as np
from PIL import Image
from glob import glob
import datetime
import sys

import numpy as np
import os

import math
from scipy import misc, ndimage
import matplotlib.pyplot as plt
import matplotlib
import cv2
import matplotlib.pyplot as plt
from skimage.data import page
from skimage.filters import (threshold_otsu, threshold_niblack,
                             threshold_sauvola)

matplotlib.rcParams['font.size'] = 9

from matplotlib import pyplot as plt

import numpy as np
import math
from math import fabs, radians, sin, cos
import matplotlib.pyplot as plt

# 引入文本纠错
from  textErrorCorrection import textErrorCorrection


# Sauvola二值化
def binary_sauvola(path):
    image = cv2.imread(path)
    gray = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    window_size = 25
    thresh_sauvola = threshold_sauvola(gray, window_size=window_size)
    binary_sauvola = gray > thresh_sauvola
    return binary_sauvola, gray, image

def rotated_img_with_fft_2(path):
    print("----------进入兜底方案-----------")
    thresh, gray, img = binary_sauvola(path)
    coords = np.column_stack(np.where(thresh < 1))

    angle = cv2.minAreaRect(coords)[-1]

    if angle < -45:
        angle = -(90 + angle)
    elif angle > 45:
        angle = 90 - angle
    else:
        angle = -angle

    print("-----------获取文本行倾斜角度------------")
    print("倾斜角angle：{}".format(angle))
    h, w = img.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(img, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    # cv2.putText(rotated, "Angle:{:.2f} degrees".format(angle), (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 255), 4)

    print("-------校正已经完成------")
    print("校正效果图地址(同级目录)为：afterCorrection.png")
    cv2.imwrite("afterCorrection.png",rotated)
    return rotated

def show(img_one, name1):
    plt.figure(figsize=(4, 4))
    plt.subplot(1, 1, 1)
    plt.imshow(img_one, cmap=plt.cm.gray)
    plt.title(name1)
    plt.axis('off')

    plt.show()
#预处理
def getCorrect(input_path):
    # 读取图片
    src = cv2.imread(input_path)
    # 非局部（Non-local）去噪
    denoise = cv2.fastNlMeansDenoisingColored(src, None, 10, 10, 7, 21)
    print("-------去噪已经完成------")
    print("去噪效果图地址(同级目录)为：afterDenoising.png")
    cv2.imwrite("afterDenoising.png",denoise)
    # 灰度化
    gray = cv2.cvtColor(denoise, cv2.COLOR_BGR2GRAY)
    # 腐蚀、膨胀
    kernel = np.ones((5, 5), np.uint8)
    erode_Img = cv2.erode(gray, kernel)
    eroDil = cv2.dilate(erode_Img, kernel)

    # 边缘检测
    canny = cv2.Canny(eroDil, 50, 150)

    # 霍夫变换得到线条
    point=src.shape[0]/3
    lines = cv2.HoughLinesP(canny, 0.8, np.pi / 180, 90, minLineLength=point, maxLineGap=10)
    drawing = np.zeros(src.shape[:], dtype=np.uint8)

    # 无线条 采用方案二
    if lines is None:
        print("----------霍夫变换并未检测到直线---------")
        return rotated_img_with_fft_2(input_path)
    print("----------霍夫变换检测到直线---------")
    for line in lines:
        x1, y1, x2, y2 = line[0]
        cv2.line(drawing, (x1, y1), (x2, y2), (0, 255, 0), 3, lineType=cv2.LINE_AA)

    """
    计算角度,因为x轴向右，y轴向下，所有计算的斜率是常规下斜率的相反数，我们就用这个斜率（旋转角度）进行旋转
    """
    x1, y1, x2, y2 = lines[0][0]
    k = float(y1 - y2) / (x1 - x2)
    thera = np.degrees(math.atan(k))


    if thera > 45:
        thera = 90 - thera
    elif thera < -45:
        thera = 90 + thera



    """
    旋转角度大于0，则逆时针旋转，否则顺时针旋转
    """
    rotateImg = rotate(src, thera)
    print("-------校正已经完成------")
    print("校正效果图地址(同级目录)为：afterCorrection.png")
    cv2.imwrite("afterCorrection.png", rotateImg)
    return rotateImg

def rotate(image, angle, center=None, scale=1.0):
    (w, h) = image.shape[0:2]
    if center is None:
        center = (w // 2, h // 2)
    wrapMat = cv2.getRotationMatrix2D(center, angle, scale)
    return cv2.warpAffine(image, wrapMat, (h, w), borderValue=(255, 255, 255))

#编码
def img_to_base64(img):
    image = cv2.imencode('.jpg', img)[1]
    image_code = str(base64.b64encode(image))[2:-1]
    return image_code

# 解码
def base64_to_img(data):
    encode = base64.b64decode(data)
    name=str((int)(time.time()))
    with open('input'+name+'.png', 'wb') as f:
        f.write(encode)
        f.close()
    return 'input'+name+'.png'

#检测与识别
def single_pic_proc(image_file):
    # 将RGBA四通道转化为三通道，A通道为透明通道
    image = np.array(Image.open(image_file).convert('RGB'))
    # ocr处理
    result, image_framed = ocr(image)
    return result, image_framed

class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)

app = Flask(__name__)


# 作用      银行回执单的检测与识别集成模块
# 方法      POST
# return   JSON格式的文本行
# url       http://127.0.0.1:9999/receiptIdentification
@app.route('/receiptIdentification', methods=['POST'])
def get_json():
    starttime = datetime.datetime.now().second
    base64Img=request.form['img']
    print("-------开始接受post请求数据：编码后的输入图片------")
    print(base64Img)
    #解码
    path=base64_to_img(base64Img)
    print("----------------解码已完成-----------------")
    #预处理
    print("--------------预处理模块启动----------------")
    n1 =getCorrect(path)
    cv2.imwrite(path, n1)
    print("--------------预处理模块已完成---------------")
    #识别
    print("----------------文本检测启动-----------------")
    result, image_framed=single_pic_proc(path)
    # print(result)
    # print(image_framed)
    ctpnPath="CTPNResult.png"
    Image.fromarray(image_framed).save(ctpnPath)
    print("---------------文本识别模块已完成---------------")
    #识别结果
    RecognitionResult=[]
    #数据处理
    print("----------------文本数据最终处理----------------")
    for key,value in result.items():
        print("-------- -----第{}个检测框---------- ----".format(key))
        print("检测框坐标：{}".format(value[0]))
        txt=textErrorCorrection(value[1])
        RecognitionResult.append(txt)
        print("检测框识别结果：{}".format(txt))
    #返回JSON格式的识别结果
    JSONResult = {"code": "200",
            "status": "success",
            "recognitionResult": []}
    JSONResult['recognitionResult']=RecognitionResult
    #记录检测与识别耗时
    endtime = datetime.datetime.now().second
    print("------------------集成模块单次耗时---------------")
    consume=abs(endtime - starttime)
    print("此银行回执单的检测与识别时间为： {}s".format(consume))
    print("-------------返回JSON格式的文本行数据---------------")
    return jsonify(JSONResult)

if __name__ == '__main__':

    app.run(host='127.0.0.1', port=9999, use_reloader=False)
