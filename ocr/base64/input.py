#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2022/3/8 13:19
# @Author  : luocan
# @E-mail  : luott_777@163.com
# @Site    : 
# @File    : input.py
# @Software: PyCharm

# 将图片转化为base64格式，以便网络传输
# 将base64格式转化为图片，方便后续使用

import base64
import cv2


def img_to_base64(img):
    image = cv2.imencode('.jpg', img)[1]
    image_code = str(base64.b64encode(image))[2:-1]
    return image_code


def base64_to_img(data):
    encode = base64.b64decode(data)
    with open('base64toImg.jpg', 'wb') as f:
        f.write(encode)
        f.close()

def imgCoding(name):
    print("--------开始编码---------")
    path=name+".png"
    src = cv2.imread(path)
    base_ = img_to_base64(src)
    print("--------Base64编码结果---------")
    print(base_)
    print("--------将编码结果存入{}.txt文件---------".format(name))
    txt=name+".txt"
    t = open(txt, 'w', encoding='utf-8')
    t.write(base_)
    t.close()
    print("--------编码结束---------")

if __name__ == '__main__':

    # 将输入图像进行base64编码
    name1="pretreatment-Test-one"
    name2="pretreatment-Test-two"
    print("--------测试1---------")
    imgCoding(name1)
    print("--------测试2---------")
    imgCoding(name2)