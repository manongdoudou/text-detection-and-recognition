#!user/bin/env python3
# -*- coding: gbk -*-
# 文本纠错


def getConfidence(line,str):
    # 置信度
    confidence=0.0
    #置信度分数
    confidenceScore=0.0
    if abs(len(line)-len(str))>1:
        return confidenceScore
    for i in range(len(str)-1):
        if line[i]==str[i]:
            confidenceScore=confidenceScore+1
    confidence=confidenceScore/len(str)
    # 规则  长度小于7 置信度百分比大于50%
    #      长度大于等于7 置信度百分比大于（len-3)/len *100
    value=(len(str)-3)/len(str)
    if  len(str)<7:
        if confidence>0.5:
            return confidence
    else:
        if confidence>value:
            return confidence
    return -1

def textErrorCorrection(str):
    #最大置信度
    curline=""
    max=-1
    f = open('confidence.txt', encoding='gbk')
    for line in f:
        line = line.replace("\n", "")
        if len(line) < 2:
            continue
        if len(str)>len(line):
            # max=(max,getConfidence(line,str[0:len(line)-1]))[max>getConfidence(line,str[0:len(line)-1])]
           if max<getConfidence(line,str[0:len(line)]):
              curline=line
              max=getConfidence(line,str[0:len(line)])
    if max==-1:
        return str
    else:
        return curline + str[len(curline):]





if __name__ == '__main__':

    str="付款人名称11"
    line=textErrorCorrection(str)

    print(line)