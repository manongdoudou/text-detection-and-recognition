# coding=gbk
import time

# 360w图像数据转化为dataset可以接收的数据格式
def main():
    time_start = time.time()
    testPath = "original_data/test.txt"
    trainPath="original_data/train.txt"
    testTargetPath = "train_data/val.txt"
    trainTargetPath = "train_data/train.txt"
    txtPath = 'original_data/label.txt'
    txt2txt(testPath, testTargetPath, txtPath)
    txt2txt(trainPath, trainTargetPath, txtPath)
    time_end = time.time()
    print('Time cost = %fs' % (time_end - time_start))


def txt2txt(sourcePath, targetPath, txtPath):
    source = open(sourcePath, encoding='gbk', errors='ignore')
    f = open(txtPath, encoding='gbk', errors='ignore')
    txt = []
    for line in f:
        txt.append(line.strip())
    lines = source.readlines()
    all_out = []
    for line in lines:
        tmp = line.strip()
        out = line3label(tmp, txt)
        all_out.append(out + "\n")
    all_out = "".join([str(x) for x in all_out])
    with open(targetPath, "w") as ff:
        ff.write(str(all_out))
        ff.close()


def line3label(line, txt):
    img_name = line.split(" ")[0]
    word = line.split(" ")[1:]
    img_path = "./train_data/data/" + img_name
    # print(img_path)
    res = ascii2cc(word, txt)
    # print(res)
    label = img_path + " " + res
    # print(label)
    return label


def ascii2cc(line, txt):
    res = ""
    for i in line:
        index = int(i)
        res += txt[index]
    return res


if __name__ == '__main__':
    main()
