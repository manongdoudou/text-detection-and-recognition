#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2022/2/24 18:56
# @Author  : luocan
# @E-mail  : luott_777@163.com
# @Site    : 
# @File    : hough_tilt_angle.py
# @Software: PyCharm
import cv2
import numpy as np
import math
from math import fabs, radians, sin, cos
import matplotlib.pyplot as plt
from binarization import binary_sauvola, binary_show,show



def rotated_img_with_fft(gray):
    # 图像延扩
    h, w = gray.shape[:2]
    # 以空间换时间，一般应该是2的n次方，这样便于FFT进行更多层次的二分，从而加快变换速度扩大数值到特定值来加速傅里叶变换
    new_h = cv2.getOptimalDFTSize(h)
    new_w = cv2.getOptimalDFTSize(w)
    right = new_w - w
    bottom = new_h - h
    # 扩充图像的边界，
    img = cv2.copyMakeBorder(gray, 0, bottom, 0, right, borderType=cv2.BORDER_CONSTANT, value=0)

    # 执行傅里叶变换，并过得频域图像
    f = np.fft.fft2(img)
    fshift = np.fft.fftshift(f)

    fft_img = np.log(np.abs(fshift))
    fft_img = (fft_img - np.amin(fft_img)) / (np.amax(fft_img) - np.amin(fft_img))

    fft_img *= 255
    ret, thresh = cv2.threshold(fft_img, 150, 255, cv2.THRESH_BINARY)

    # 霍夫直线变换
    thresh = img.astype(np.uint8)
    lines = cv2.HoughLinesP(img, 1, np.pi / 180, 30, minLineLength=40, maxLineGap=100)
    try:
        lines1 = lines[:, 0, :]
    except Exception as e:
        lines1 = []

    piThresh = np.pi / 180
    pi2 = np.pi / 2
    angle = 0
    for line in lines1:
        x1, y1, x2, y2 = line
        if x2 - x1 == 0:
            continue
        else:
            theta = (y2 - y1) / (x2 - x1)
        if abs(theta) < piThresh or abs(theta - pi2) < piThresh:
            continue
        else:
            angle = abs(theta)
            break

    angle = math.atan(angle)
    angle = angle * (180 / np.pi)

    center = (w // 2, h // 2)
    height_1 = int(w * fabs(sin(radians(angle))) + h * fabs(cos(radians(angle))))
    width_1 = int(h * fabs(sin(radians(angle))) + w * fabs(cos(radians(angle))))

    M = cv2.getRotationMatrix2D(center, angle, 1.0)

    M[0, 2] += (width_1 - w) / 2
    M[1, 2] += (height_1 - h) / 2
    rotated = cv2.warpAffine(gray, M, (width_1, height_1), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)

    return rotated


def rotated_img_with_fft_2(path):
    thresh, gray, img = binary_sauvola(path)
    binary_show(thresh, "sauvola-thresh", gray, " gray", img, "img")

    show(thresh, " ")

    # print(thresh)
    coords = np.column_stack(np.where(thresh < 1))

    angle = cv2.minAreaRect(coords)[-1]
    print(angle)

    if angle < -45:
        angle = -(90 + angle)
    elif angle > 45:
        angle = 90 - angle
    else:
        angle = -angle
    h, w = img.shape[:2]
    center = (w // 2, h // 2)
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(img, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE)
    # cv2.putText(rotated, "Angle:{:.2f} degrees".format(angle), (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 1.5, (0, 0, 255), 4)
    print("[INFO] angle:{:.3f}".format(angle))

    # plt.figure(figsize=(8, 7))
    plt.figure(figsize=(4, 4))
    plt.subplot(1, 1, 1)
    plt.imshow(rotated, cmap=plt.cm.gray)
    plt.title(' ')
    plt.axis('off')

    # plt.subplot(1, 2, 2)
    # plt.imshow(img, cmap=plt.cm.gray)
    # plt.title('Original image')
    # plt.axis('off')

    plt.show()
    cv2.imwrite("r1.png",rotated)

    return rotated


if __name__ == "__main__":
    input_path = "input_photo/img2.png"
    rotated_img_with_fft_2(input_path)
    #
    # plt.figure(figsize=(8, 7))
    # plt.subplot(1, 2, 1)
    # plt.imshow(fft, cmap=plt.cm.gray)
    # plt.title('Hougt image')
    # plt.axis('off')
    #
    # plt.subplot(1, 2, 2)
    # plt.imshow(image, cmap=plt.cm.gray)
    # plt.title('Original image')
    # plt.axis('off')
    #
    # plt.show()
